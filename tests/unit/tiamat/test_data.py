from unittest import mock


def test_version(hub, mock_hub, bname):
    mock_hub.tiamat.data.version = hub.tiamat.data.version
    mock_hub.tiamat.BUILDS[bname].dir = ""
    mock_hub.tiamat.BUILDS[bname].name = ""

    with mock.patch("os.path.isfile", return_value=True), mock.patch(
        "builtins.open", mock.mock_open(read_data="version='99'")
    ):
        mock_hub.tiamat.data.version(bname)

    assert mock_hub.tiamat.BUILDS[bname].version == "99"
