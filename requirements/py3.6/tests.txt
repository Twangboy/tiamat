#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile -o requirements/py3.6/tests.txt -v requirements/tests.in
#
aiofiles==0.4.0           # via aiologger
aiologger[aiofiles]==0.6.0  # via pop-config
asynctest==0.13.0         # via pytest-pop
attrs==20.2.0             # via pytest
certifi==2020.6.20        # via requests
chardet==3.0.4            # via requests
dict-toolbox==1.16
idna==2.10                # via requests
importlib-metadata==2.0.0  # via pluggy, pytest
iniconfig==1.0.1          # via pytest
mock==4.0.2               # via pytest-pop
more-itertools==8.5.0     # via pytest
msgpack==1.0.0            # via dict-toolbox, pop, pytest-salt-factories
packaging==20.4           # via pytest
pluggy==0.13.1            # via pytest
pop-config==6.10          # via pop, pytest-pop
pop==14
psutil==5.7.2             # via pytest-salt-factories
py==1.9.0                 # via pytest
pyparsing==2.4.7          # via packaging
pytest-asyncio==0.14.0    # via pytest-pop
pytest-pop==6.3
pytest-salt-factories==0.13.0  # via pytest-pop
pytest-tempdir==2019.10.12  # via pytest-salt-factories
pytest==6.0.2
pyyaml==5.3.1             # via dict-toolbox, pop
pyzmq==19.0.2             # via pytest-salt-factories
requests==2.24.0
six==1.15.0               # via packaging
toml==0.10.1              # via pytest
urllib3==1.25.10          # via requests
zipp==3.2.0               # via importlib-metadata

# The following packages are considered to be unsafe in a requirements file:
# setuptools
