"""
Test the build process and execute the resulting binary to ensure correct output.
"""
import os
import platform
from ast import literal_eval
from unittest import mock


def test_build(hub, pb_bin, pb_conf):
    with mock.patch(
        "sys.argv",
        [
            "tiamat/run.py",
            "--log-level=debug",
            "build",
            "--config",
            pb_conf,
            "--exclude",
            "aiologger",
            "aiofiles",
            "pycparser",
            "pyinstaller",
            "pyinstaller-hooks-contrib",
            "setuptools",
            "wheel",
            "--pyinstaller-args",
            "'--strip'",
        ],
    ):
        hub.tiamat.init.cli()

    cp = hub.tiamat.cmd.run(pb_bin)

    assert not cp.retcode, cp.stderr
    assert "pb works!" in cp.stdout
    assert "hashids works!" in cp.stdout
    assert "bodger" in cp.stdout


def test_special_cases(hub, data_dir, sc_bin):
    with mock.patch(
        "sys.argv",
        [
            "tiamat/run.py",
            "--log-level=debug",
            "build",
            "--run",
            str(data_dir / "sc" / "special_cases.py"),
            "--requirements",
            str(data_dir / "sc" / "requirements.txt"),
            "--name",
            "special.bin",
            "--exclude",
            "aiologger",
            "aiofiles",
            "pycparser",
            "pyinstaller",
            "pyinstaller-hooks-contrib",
            "setuptools",
            "six",
            "wheel",
            "--pyinstaller-args",
            "'--strip'",
        ],
    ):
        hub.tiamat.init.cli()

    cp = hub.tiamat.cmd.run(sc_bin)

    assert not cp.retcode, cp.stderr

    result = literal_eval(cp.stdout)

    # pyinstaller pr5649 __file__ now returns absolute path
    assert os.path.basename(result["__file__"]) == "special.py"
    assert result["os.path.abspath(__file__)"].startswith("/tmp/_MEI")

    assert result["asyncio.create_subprocess_exec"] == "async exec"
    assert result["asyncio.create_subprocess_shell"] == "async shell"
    assert result["os.getcwd()"] == str(data_dir.joinpath("sc"))
    assert result["platform.system()"] == platform.system()
    assert result["platform.uname()"] == tuple(platform.uname())
    assert result["subprocess.Popen"] == "Popen"
    assert result["sys._MEIPASS"].startswith("/tmp/_MEI")
    assert result["sys.argv[0]"] == sc_bin
    assert result["sys.executable"] == sc_bin
    assert result["sys.frozen"] is True
