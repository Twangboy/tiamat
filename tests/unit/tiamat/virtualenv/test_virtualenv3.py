def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.virtualenv3.bin = hub.tiamat.virtualenv.virtualenv3.bin
    mock_hub.tiamat.virtualenv.virtualenv3.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.virtualenv3.create = (
        hub.tiamat.virtualenv.virtualenv3.create
    )
    mock_hub.tiamat.virtualenv.virtualenv3.create(bname)
